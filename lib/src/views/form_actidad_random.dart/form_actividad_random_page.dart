import 'package:flutter/material.dart';
import 'package:list_to_do/src/data/apis/api_activities_random.dart';
import 'package:list_to_do/src/data/helpers/actividades_helper.dart';
import 'package:list_to_do/src/data/providers/form_activity_provider.dart';
import 'package:list_to_do/src/util/style.dart';
import 'package:list_to_do/src/views/form_actividad/card_formulario.dart';
import 'package:provider/provider.dart';

class FormActividadRandom extends StatefulWidget {
  FormActividadRandom({Key? key}) : super(key: key);

  @override
  _FormActividadRandomState createState() => _FormActividadRandomState();
}

class _FormActividadRandomState extends State<FormActividadRandom> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Actividades Aleatorias"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CardFormulario(
              child: ChangeNotifierProvider(
                child: _FormManyActivities(),
                create: (_) => FormActivityProvider(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _FormManyActivities extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final validForm = Provider.of<FormActivityProvider>(context);
    final actividadHelper = Provider.of<ActividadesHelper>(context);
    int cantidad = 0;

    return Container(
      child: Form(
        key: validForm.formKey,
        child: Column(
          children: [
            TextFormField(
              keyboardType: TextInputType.number,
              decoration: Style.decoracionInput(
                hintText: "Cantidad de actividades",
                labelText: "Cantidad",
              ),
              validator: (value) => value == '' ? "Campo Requerido" : null,
              onChanged: (value) => cantidad = int.parse(value),
            ),
            SizedBox(
              height: 20,
            ),
            MaterialButton(
              onPressed: () {
                if (!validForm.isValidForm()) return;
                ApiActivitiesRandom().loadFacts(cantidad, actividadHelper);
                Navigator.pop(context);
              },
              padding: EdgeInsets.symmetric(horizontal: 80, vertical: 12),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                "GUARDAR",
                style: TextStyle(
                    color: Colors.white, fontFamily: 'OpenSans', fontSize: 15),
              ),
              color: Colors.blue,
            )
          ],
        ),
      ),
    );
  }
}
