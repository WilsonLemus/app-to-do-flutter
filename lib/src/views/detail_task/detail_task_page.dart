import 'package:flutter/material.dart';
import 'package:list_to_do/src/data/helpers/actividades_helper.dart';
import 'package:list_to_do/src/data/providers/form_activity_provider.dart';
import 'package:list_to_do/src/util/style.dart';
import 'package:list_to_do/src/views/form_actividad/card_formulario.dart';
import 'package:provider/provider.dart';
import 'package:list_to_do/src/util/globals.dart' as global;

class DetailTaskPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detalles Actividad"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            CardFormulario(
              child: ChangeNotifierProvider(
                create: (_) => FormActivityProvider(),
                child: _FormNewActivity(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _FormNewActivity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final validForm = Provider.of<FormActivityProvider>(context);
    final actividadHelper = Provider.of<ActividadesHelper>(context);

    return Container(
      child: Form(
        key: validForm.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            TextFormField(
              autocorrect: true,
              decoration: Style.decoracionInput(
                  hintText: "Titulo de la actividad",
                  labelText: "Titulo",
                  prefixIcon: Icons.title),
              validator: (value) => value == '' ? "Campo Requerido" : null,
              onChanged: (value) => global.actividadDetalle.titulo = value,
              initialValue: global.actividadDetalle.titulo,
              enabled: false,
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              maxLines: 8,
              autocorrect: true,
              decoration: Style.decoracionInput(
                  hintText: "Descripcion de la actividad",
                  labelText: "Descripcion",
                  prefixIcon: Icons.subtitles),
              validator: (value) => value == '' ? "Campo Requerido" : null,
              onChanged: (value) => global.actividadDetalle.descripcion = value,
              initialValue: global.actividadDetalle.descripcion,
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              autocorrect: true,
              decoration: Style.decoracionInput(
                  hintText: "Estado de la actividad",
                  labelText: "Estado",
                  prefixIcon: Icons.stars_outlined),
              validator: (value) => value == '' ? "Campo Requerido" : null,
              onChanged: (value) => global.actividadDetalle.descripcion = value,
              initialValue: global.actividadDetalle.estado,
              enabled: false,
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton.icon(
              icon: Icon(
                Icons.save,
                color: Colors.white,
              ),
              onPressed: () {
                if (!validForm.isValidForm()) return;
                actividadHelper.updateActivityById(global.actividadDetalle);
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  primary: Colors.blue,
                  padding: EdgeInsets.symmetric(horizontal: 75, vertical: 12)),
              label: Text("Editar",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 15)),
            ),
            SizedBox(
              height: 10,
            ),
            ElevatedButton.icon(
              icon: Icon(
                Icons.check,
                color: Colors.white,
              ),
              onPressed: () {
                if (!validForm.isValidForm()) return;
                global.actividadDetalle.estado = "Realizada";
                actividadHelper.updateActivityById(global.actividadDetalle);
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  primary: Colors.green[700],
                  padding: EdgeInsets.symmetric(horizontal: 70, vertical: 12)),
              label: Text("Realizar",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 15)),
            ),
            SizedBox(
              height: 10,
            ),
            ElevatedButton.icon(
              icon: Icon(
                Icons.delete,
                color: Colors.white,
              ),
              onPressed: () {
                if (!validForm.isValidForm()) return;
                actividadHelper.deleteActivity(global.actividadDetalle);
                Navigator.pop(context);
              },
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  primary: Colors.red,
                  padding: EdgeInsets.symmetric(horizontal: 70, vertical: 12)),
              label: Text("Eliminar",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'OpenSans',
                      fontSize: 15)),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
