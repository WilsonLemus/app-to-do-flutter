import 'package:flutter/material.dart';
import 'package:list_to_do/src/data/helpers/actividades_helper.dart';
import 'package:list_to_do/src/data/helpers/db_helper.dart';
import 'package:list_to_do/src/models/entities/actividad.dart';
import 'package:list_to_do/src/views/list_to_do/floating_button.dart';
import 'package:list_to_do/src/views/list_to_do/list_activities.dart';
import 'package:provider/provider.dart';

class ListToDoPage extends StatefulWidget {
  ListToDoPage({Key? key}) : super(key: key);

  @override
  _ListToDoPageState createState() => _ListToDoPageState();
}

class _ListToDoPageState extends State<ListToDoPage> {
  int index = 0;
  List<Actividad> todasActividades = [];
  bool loadInitial = true;
  int cantidad = 0;

  @override
  void initState() {
    super.initState();
    DBHelper.db.database;
  }

  @override
  Widget build(BuildContext context) {
    final actividadHelper = Provider.of<ActividadesHelper>(context);

    if (loadInitial) {
      actividadHelper.loadAllActivities();
      loadInitial = false;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Actividades'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, 'RandomTask');
            },
            icon: Icon(Icons.library_add_outlined),
          )
        ],
      ),
      body: Column(
        children: [
          TextField(
            decoration: InputDecoration(
              hintText: "Busqueda...",
              contentPadding: EdgeInsets.all(15.0),
            ),
            onChanged: (value) {
              actividadHelper.loadActivityByDesc(value);
            },
          ),
          Expanded(
            child: ListActivities(),
          ),
        ],
      ),
      floatingActionButton: AddFloatingButton(),
    );
  }
}
