import 'package:flutter/material.dart';
import 'package:list_to_do/src/data/helpers/actividades_helper.dart';
import 'package:provider/provider.dart';
import 'package:list_to_do/src/util/globals.dart' as global;

class ListActivities extends StatelessWidget {
  const ListActivities({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final actividadHelper = Provider.of<ActividadesHelper>(context);
    //actividadHelper.loadAllActivities();
    final act = actividadHelper.actividades;

    return ListView.builder(
      itemCount: act.length,
      itemBuilder: (_, i) => ListTile(
        leading: Icon(
          Icons.local_activity_outlined,
          color: act[i].estado == 'Realizada' ? Colors.green : Colors.blue,
        ),
        title: Text(act[i].titulo),
        subtitle: Text(act[i].descripcion),
        onTap: () {
          global.actividadDetalle = act[i];
          Navigator.pushNamed(context, "DetailTask");
        },
        onLongPress: () => print('eliminar'),
        trailing: Icon(
          Icons.keyboard_arrow_right,
          color: Colors.grey,
        ),
      ),
    );
  }
}
