import 'package:flutter/material.dart';
import 'package:list_to_do/src/util/style.dart';

class CardFormulario extends StatelessWidget {
  final Widget child;

  const CardFormulario({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20),
      child: Container(
        padding: EdgeInsets.all(15),
        width: double.infinity,
        decoration: Style.decoracionCaja(),
        child: this.child,
      ),
    );
  }
}
