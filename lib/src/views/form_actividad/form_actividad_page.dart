import 'package:flutter/material.dart';
import 'package:list_to_do/src/data/helpers/actividades_helper.dart';
import 'package:list_to_do/src/data/providers/form_activity_provider.dart';
import 'package:list_to_do/src/util/style.dart';
import 'package:list_to_do/src/views/form_actividad/card_formulario.dart';
import 'package:provider/provider.dart';
import 'package:list_to_do/src/util/globals.dart' as global;

class FormActividadPage extends StatefulWidget {
  FormActividadPage({Key? key}) : super(key: key);

  @override
  _FormActividadPageState createState() => _FormActividadPageState();
}

class _FormActividadPageState extends State<FormActividadPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Nueva Actividad"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            CardFormulario(
              child: ChangeNotifierProvider(
                create: (_) => FormActivityProvider(),
                child: _FormNewActivity(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _FormNewActivity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final validForm = Provider.of<FormActivityProvider>(context);
    final actividadHelper = Provider.of<ActividadesHelper>(context);

    return Container(
      child: Form(
        key: validForm.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Column(
          children: [
            TextFormField(
              autocorrect: true,
              decoration: Style.decoracionInput(
                  hintText: "Titulo de la actividad",
                  labelText: "Titulo",
                  prefixIcon: Icons.title),
              validator: (value) => value == '' ? "Campo Requerido" : null,
              onChanged: (value) => global.actividadDetalle.titulo = value,
            ),
            SizedBox(
              height: 20,
            ),
            TextFormField(
              maxLines: 8,
              autocorrect: true,
              decoration: Style.decoracionInput(
                  hintText: "Descripcion de la actividad",
                  labelText: "Descripcion",
                  prefixIcon: Icons.subtitles),
              validator: (value) => value == '' ? "Campo Requerido" : null,
              onChanged: (value) => global.actividadDetalle.descripcion = value,
            ),
            SizedBox(
              height: 20,
            ),
            MaterialButton(
              onPressed: () {
                if (!validForm.isValidForm()) return;
                actividadHelper.addActivity(global.actividadDetalle);
                Navigator.pop(context);
              },
              padding: EdgeInsets.symmetric(horizontal: 80, vertical: 12),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                "GUARDAR",
                style: TextStyle(
                    color: Colors.white, fontFamily: 'OpenSans', fontSize: 15),
              ),
              color: Colors.blue,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              '* El estado de la nueva actividad sera "Abierto"',
              style: TextStyle(fontFamily: 'OpenSans', color: Colors.black45),
            ),
          ],
        ),
      ),
    );
  }
}
