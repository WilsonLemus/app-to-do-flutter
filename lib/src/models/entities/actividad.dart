class Actividad {
  String titulo;
  String descripcion;
  String estado;
  int id;

  Actividad(
      {required this.id,
      required this.titulo,
      required this.descripcion,
      required this.estado});

  Map<String, dynamic> toMap() =>
      {"titulo": titulo, "descripcion": descripcion, "estado": estado};

  factory Actividad.fromMap(Map<String, dynamic> map) => Actividad(
      id: map['id'],
      titulo: map['titulo'],
      descripcion: map['descripcion'],
      estado: map['estado']);
}
