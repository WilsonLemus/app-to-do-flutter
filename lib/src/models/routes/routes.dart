import 'package:flutter/material.dart';
import 'package:list_to_do/src/views/detail_task/detail_task_page.dart';
import 'package:list_to_do/src/views/form_actidad_random.dart/form_actividad_random_page.dart';
import 'package:list_to_do/src/views/form_actividad/form_actividad_page.dart';
import 'package:list_to_do/src/views/list_to_do/list_to_do_page.dart';

Map<String, WidgetBuilder> getApplicationRoute() {
  return <String, WidgetBuilder>{
    '/': (BuildContext context) => ListToDoPage(),
    'NewTask': (BuildContext context) => FormActividadPage(),
    'DetailTask': (BuildContext context) => DetailTaskPage(),
    'RandomTask': (BuildContext context) => FormActividadRandom(),
  };
}
