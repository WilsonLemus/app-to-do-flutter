import 'package:flutter/material.dart';

class Style {
  static BoxDecoration decoracionCaja() {
    return BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [
          BoxShadow(
              color: Colors.black12, blurRadius: 6.0, offset: Offset(0, 2)),
        ]);
  }

  static InputDecoration decoracionInput(
      {required String hintText,
      required String labelText,
      IconData? prefixIcon}) {
    return InputDecoration(
      hintText: hintText,
      hintStyle: TextStyle(
        fontFamily: 'OpenSans',
        color: Colors.black26,
      ),
      labelText: labelText,
      labelStyle: TextStyle(
          color: Colors.black54,
          fontFamily: 'OpenSans',
          fontSize: 20,
          fontWeight: FontWeight.bold),
      prefixIcon: prefixIcon != null ? Icon(prefixIcon) : null,
    );
  }
}
