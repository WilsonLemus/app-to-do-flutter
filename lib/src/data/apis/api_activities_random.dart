import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:list_to_do/src/data/helpers/actividades_helper.dart';
import 'package:list_to_do/src/models/entities/actividad.dart';

class ApiActivitiesRandom extends ChangeNotifier {
  final String _baseUri = 'https://catfact.ninja/fact';
  late Actividad _actividad =
      new Actividad(id: 0, titulo: '', descripcion: '', estado: 'Abierto');

  Future loadFacts(int cantidad, ActividadesHelper actividadHelper) async {
    final url = Uri.parse(_baseUri);
    for (int i = 0; i < cantidad; i++) {
      final response = await http.get(url);
      if (response.statusCode == 200) {
        String _body = utf8.decode(response.bodyBytes);
        final jsonData = json.decode(_body);
        _actividad.titulo = "Random ${jsonData['length'].toString()}";
        _actividad.descripcion = jsonData['fact'];
        actividadHelper.addActivity(_actividad);
      }
    }
  }
}
