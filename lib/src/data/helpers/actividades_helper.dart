import 'package:flutter/material.dart';
import 'package:list_to_do/src/data/helpers/db_helper.dart';
import 'package:list_to_do/src/models/entities/actividad.dart';
import 'package:list_to_do/src/util/globals.dart' as global;

class ActividadesHelper extends ChangeNotifier {
  List<Actividad> actividades = [];
  DBHelper dbHelper = DBHelper.db;

  addActivity(Actividad actividad) async {
    final idRes = await this.dbHelper.insert(actividad);
    actividad.id = idRes;
    this.loadAllActivities();
  }

  loadActivityById(int id) async {
    Actividad actividad = await this.dbHelper.getActividadById(id);
    global.actividadDetalle = actividad;
  }

  loadAllActivities() async {
    final actividades = await this.dbHelper.getAllActividades();
    this.actividades = [...actividades];
    notifyListeners();
  }

  loadActivityByDesc(String desc) async {
    final actividades = await this.dbHelper.getActividadByDescription(desc);
    this.actividades = [...actividades];
    notifyListeners();
  }

  updateActivityById(Actividad actividad) {
    this.dbHelper.update(actividad);
    this.loadAllActivities();
  }

  deleteActivity(Actividad actividad) {
    dbHelper.delete(actividad.id);
    this.loadAllActivities();
  }

  deleteAllActivity() {
    dbHelper.deleteAll();
    this.actividades = [];
    notifyListeners();
  }
}
