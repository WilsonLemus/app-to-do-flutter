import 'dart:io';

import 'package:list_to_do/src/models/entities/actividad.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';

class DBHelper {
  static dynamic _database;
  static final DBHelper db = DBHelper._();
  DBHelper._();

  Future<Database> get database async {
    if (_database != null) return _database;

    _database = await _initDB();
    return _database;
  }

  Future<Database> _initDB() async {
    Directory documentsDyrectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDyrectory.path, 'Task.db');
    print("*************************************************************");
    print(path);

    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (db, version) async {
        await db.execute('''
          CREATE TABLE tasks(
            id INTEGER PRIMARY KEY,
            titulo TEXT,
            descripcion TEXT,
            estado TEXT
          )
        ''');
      },
    );
  }

  Future<int> insert(Actividad actividad) async {
    final dbx = await database;
    final res = await dbx.insert("tasks", actividad.toMap());
    print('Insertar $res');
    return res;
  }

  Future<Actividad> getActividadById(int id) async {
    final dbx = await database;
    final res = await dbx.query('tasks', where: 'id = ?', whereArgs: [id]);

    return res.isNotEmpty
        ? Actividad.fromMap(res.first)
        : new Actividad(id: 0, titulo: '', descripcion: '', estado: '');
  }

  Future<List<Actividad>> getAllActividades() async {
    final dbx = await database;
    final res = await dbx.query('tasks');

    return res.isNotEmpty ? res.map((e) => Actividad.fromMap(e)).toList() : [];
  }

  Future<List<Actividad>> getActividadByDescription(String desc) async {
    final dbx = await database;
    final res = await dbx
        .query('tasks', where: "descripcion like ?", whereArgs: ['%$desc%']);

    return res.isNotEmpty ? res.map((e) => Actividad.fromMap(e)).toList() : [];
  }

  Future<int> update(Actividad actividad) async {
    Database db = await database;
    return await db.update('tasks', actividad.toMap(),
        where: 'id = ?', whereArgs: [actividad.id]);
  }

  Future<int> delete(int id) async {
    Database db = await database;
    return await db.delete('tasks', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> deleteAll() async {
    Database db = await database;
    return await db.delete('tasks');
  }
}
