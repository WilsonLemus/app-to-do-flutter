import 'package:flutter/material.dart';
import 'package:list_to_do/src/data/helpers/actividades_helper.dart';
import 'package:list_to_do/src/models/routes/routes.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => new ActividadesHelper())
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'L&A Electronic',
        initialRoute: "/",
        routes: getApplicationRoute(),
      ),
    );
  }
}
