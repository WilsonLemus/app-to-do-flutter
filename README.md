# Aplicacion de Actividades

_Esta es una aplicacion para la gestion de actividades, desarrollada con Flutter._
Esta aplicacion cuenta con: 
1. Almacenamiento interno por medio de la libreria Sqflite
2. Conexion con Api de https:catfact.ninja/docs

# Instalacion del Proyecto Visual Studio Code
- Realizar la clonacion del proyecto
- Abrir el archivo **_pubspec.yaml_**
- Realizar "Ctrl + S" o "cmd + s" para realizar la instalacion de las dependencias

# Instalacion APK Debug
- Descargar el [Apk Debug](source_app/app-debug.apk) 
- Habilitar "Instalacion de fuentes desconocidas" en el dispositivo movil
- Instalar la app

# Visualizacion de la App

### Pantalla Inicial
En la pantalla inicial encontrara el listado de actividades que existan en la aplicacion, que se encuentren almacenadas en la base de datos interna.

El icono de cada item, nos muestra el estado de la actividad. Azul nos indica que la actividad esta "Abierta" y Verde nos indica que la aplicacion esta "Realizada".

En la parte inferior derecha encontramos el boton para agregar una actividad de forma manual, abrira el formulario de ingreso.

En la parte superior derecha encontramos el boton para agregar actividades de forma masiva por medio del consumo de la Api

En la parte superior de la lista encontramos en campo para filtrar la lista por medio de la descripcion

![Screenshots](source_app/Screenshots/Pantalla_Ppal.jpeg) ![Screenshots](source_app/Screenshots/filtrar.jpeg) 

### Formulario Agregar Actividad
Se debe ingresar la informacion basica de la actividad; titulo y descripcion.
![Screenshots](source_app/Screenshots/Pantalla_Nuevo.jpeg) 

### Agregar Actividades Api
Se debe ingresar la cantidad de actividades que se desean agergar a la lista
![Screenshots](source_app/Screenshots/Pantalla_aleatoria.jpeg) 

### Acciones
En esta pantalla se puede realizar la visualizacion de la actividad con su detalle. Ademas permite: 

1. Editar la descripcion, por medio del Boton _Editar_ 
2. Asignar el estado **Realizado** por medio del boton _Realizar_
3. Eliminar la actividad, por medio del Boton _Eliminar_

![Screenshots](source_app/Screenshots/Pantalla_view_edit.jpeg) 
